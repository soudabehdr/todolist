(function () {
    'use strict';

    userProfileApps
        .controller('homeCtrl', homeOption);

    homeOption.$inject = ['$scope', 'userProfileService', 'SweetAlert', '$window'];

    function homeOption($scope, userProfileService, SweetAlert, $window) {

        var vm = this;

        //variables
        vm.postUser = postUser;


        //functions


        function postUser() {
            userProfileService.postUserInf(function (response) {

                if (response.data.length !== 0) {

                    console.log("accessToken", response.data.access_token);  // ye object mide

                    SweetAlert.swal({
                        title: 'شما با موفقیت وارد شدید',
                        text: '',
                        confirmButtonColor: "#bebbb8",
                        confirmButtonText: "بستن",
                        type: "warning"
                    });

                    var accessToken = response.data.access_token;

                    userProfileService.userProfile(accessToken, function (response) {
                        console.log("yani b injaam nmireseee???");
                        if (response.data.length !== 0) {
                            console.log("be inja mirese ayaaaa??");
                            $window.location.href = "http://localhost:3000/apps/userProfile/userProfileIndex.html#!/";
                        }
                    })

                } else {
                    SweetAlert.swal({
                        title: 'ورود با خطا مواجه شد',
                        text: '',
                        confirmButtonColor: "#bebbb8",
                        confirmButtonText: "بستن",
                        type: "warning"
                    })
                }
            })
        }
    }
})();
