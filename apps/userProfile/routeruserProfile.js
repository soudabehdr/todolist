(function () {
    'use strict';
    userProfileApps
        .config(["$routeProvider", "$locationProvider", function ($routeProvider, $browser) {


            $routeProvider
                .when('/', {
                    templateUrl: '/apps/userProfile/homePage/home.html',
                    label: 'خانه',

                })
                .when('/myFile', {
                    templateUrl: '/apps/userProfile/component/myFile.html',
                    label: 'خانه',

                })

                .otherwise({
                    redirectTo: '/'
                });
        }])

})();
