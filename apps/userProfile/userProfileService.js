(function () {
    'use strict';
    userProfileApps.factory('userProfileService', ['$http', 'localStorageService', '$rootScope', '$routeParams', '$location', '$filter',
        function ($http, localStorageService, $rootScope, $routeParams, $location, $filter) {

            const client_id = "fcc0c73f662c0135a4ee";
            const wunderlistUrl = "https://www.wunderlist.com/oauth";
            const wunderlistUser="https://a.wunderlist.com/api/v1";
            const redirect_uri = "http://localhost:3000";
            const state = "drep";
            const client_secret = "46a812edb3dd891c299fdc99d375d347fd81759866c44aaf7a413a3f9afd";



            return ({

                postUserInf: postUserInf,
                corsAllow: corsAllow,
                userProfile:userProfile

            });



            //functions

            function postUserInf() {

                corsAllow();

                /* var absUrl = $location.absUrl();
                 console.log(absUrl);*/

                var code = $location.search().code;


                var data = {
                    client_id: client_id,
                    client_secret: client_secret,
                    code: code
                };

                var config = {
                    headers: {
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*'

                    }
                };


                $http.post(wunderlistUrl + "/access_token", data, config).then(function (response) {
                    console.log("response for post", response);

                }, function (response) {
                    console.log("reject api"+response)
                }.bind(this));

            }

            function corsAllow() {
                var cors_api_host = 'cors-anywhere.herokuapp.com';
                var cors_api_url = 'https://' + cors_api_host + '/';
                var slice = [].slice;
                var origin = window.location.protocol + '//' + window.location.host;
                var open = XMLHttpRequest.prototype.open;
                XMLHttpRequest.prototype.open = function () {
                    var args = slice.call(arguments);
                    var targetOrigin = /^https?:\/\/([^\/]+)/i.exec(args[1]);
                    if (targetOrigin && targetOrigin[0].toLowerCase() !== origin &&
                        targetOrigin[1] !== cors_api_host) {
                        args[1] = cors_api_url + args[1];
                    }
                    return open.apply(this, args);
                };
            }

            function userProfile(accessToken,callback) {


                console.log("*********",accessToken);

                var config = {
                    headers: {

                        'Access-Control-Allow-Origin':'*',
                        'X-Access-Token':accessToken,
                        'X-Client-ID':client_id

                    }
                };

                $http.get(wunderlistUser + '/user',config).then(function (response) {
                    console.log("response for userProfile:",response);
                    console.log("vaaaa");
                    return callback(response);


                });


            }

        }])
})();


