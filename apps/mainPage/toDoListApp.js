var toDoListApps=angular.module('toDoListApp', [
    'ngRoute',
    'ngAnimate',
    'ngAria',
    'ngSanitize',
    'angular-bootstrap-select',
    'ngTouch',
    'LocalStorageModule',
    'oitozero.ngSweetAlert',
    'hl.sticky'
]).config(['$locationProvider', function($locationProvider){
    $locationProvider.html5Mode(true);
}]);

