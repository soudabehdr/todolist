(function () {
    'use strict';
    toDoListApps
        .config(["$routeProvider","$locationProvider",function ($routeProvider,$browser) {


            $routeProvider
                .when('/',{
                    templateUrl:'/apps/mainPage/homePage/home.html',
                    label:'خانه',

                })

                .when('/man',{
                    templateUrl:'/apps/mainPage/man.html',
                    label:'خانه',

                })

                .otherwise({
                    redirectTo: '/'
                });
        }])

})();
